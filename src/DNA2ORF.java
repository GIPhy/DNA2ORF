/*
  ########################################################################################################

  DNA2ORF: translating the maximum number of codons using the minimum number of ORFs
  
  Copyright (C) 2021  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                 research.pasteur.fr/team/hub-giphy
   Dpt. Biologie Computationnelle            research.pasteur.fr/team/bioinformatics-and-biostatistics-hub
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.zip.*;

public class DNA2ORF {

    //### options ############
    final static String VERSION = "1.0.211207ac";
    final static String[] CODON =   {"AAA" , "AAC" , "AAG" , "AAT" ,  // A A -
				     "ACA" , "ACC" , "ACG" , "ACT" ,  // . C -
				     "AGA" , "AGC" , "AGG" , "AGT" ,  // . G -
				     "ATA" , "ATC" , "ATG" , "ATT" ,  // . T -
				     "CAA" , "CAC" , "CAG" , "CAT" ,  // C A -
				     "CCA" , "CCC" , "CCG" , "CCT" ,  // . C -
				     "CGA" , "CGC" , "CGG" , "CGT" ,  // . G -
				     "CTA" , "CTC" , "CTG" , "CTT" ,  // . T -
				     "GAA" , "GAC" , "GAG" , "GAT" ,  // G A -
				     "GCA" , "GCC" , "GCG" , "GCT" ,  // . C -
				     "GGA" , "GGC" , "GGG" , "GGT" ,  // . G -
				     "GTA" , "GTC" , "GTG" , "GTT" ,  // . T -
				     "TAA" , "TAC" , "TAG" , "TAT" ,  // T A -
				     "TCA" , "TCC" , "TCG" , "TCT" ,  // . C -
				     "TGA" , "TGC" , "TGG" , "TGT" ,  // . G -
				     "TTA" , "TTC" , "TTG" , "TTT" }; // . T -
    final static char[] AMINO_ACID = {'K'  ,  'N'  ,  'K'  ,  'N'  ,  // A A -
				      'T'  ,  'T'  ,  'T'  ,  'T'  ,  // . C -
				      'R'  ,  'S'  ,  'R'  ,  'S'  ,  // . G -
				      'I'  ,  'I'  ,  'M'  ,  'I'  ,  // . T -  ('M' = START)
				      'Q'  ,  'H'  ,  'Q'  ,  'H'  ,  // C A -
				      'P'  ,  'P'  ,  'P'  ,  'P'  ,  // . C -
				      'R'  ,  'R'  ,  'R'  ,  'R'  ,  // . G -
				      'L'  ,  'L'  ,  'L'  ,  'L'  ,  // . T -
				      'E'  ,  'D'  ,  'E'  ,  'D'  ,  // G A -
				      'A'  ,  'A'  ,  'A'  ,  'A'  ,  // . C -
				      'G'  ,  'G'  ,  'G'  ,  'G'  ,  // . G -
				      'V'  ,  'V'  ,  'V'  ,  'V'  ,  // . T -
				      '?'  ,  'Y'  ,  '?'  ,  'Y'  ,  // T A -  ('?' for STOP)
				      'S'  ,  'S'  ,  'S'  ,  'S'  ,  // . C -
				      '?'  ,  'C'  ,  'W'  ,  'C'  ,  // . G -  ('?' for STOP)
				      'L'  ,  'F'  ,  'L'  ,  'F'  }; // . T -
    final static String NOTHING = "N.o./.T.h.I.n.G";
    static final int BUFFER_SIZE = 1<<16;
    final static String EOF = ">E..o..F";

    //### options ############
    static File infile;       // -i
    static File outfile;      // -o
    static int minlgt;        // -l

    //### io #################
    static BufferedReader in;
    static BufferedWriter out;

    //### data ###############
    static String lbl;
    static String seq;
    static int lgt;
    static BitSet orfed;
    static TreeSet<ORF> orfSet;
 
    //###  stuffs ############
    static int l, o, s, p, se, ss, ss1, ss2, ss3, se1, se2, se3;
    static char cs, cs_1, cs_2;
    static ORF orf;
    static String cod, line, filename;
    static StringBuilder sb;
    
    public static void main (String[] args) throws IOException {

	// ########################################################################
	// ##### documentation                                                #####
	// ########################################################################
	if ( args.length < 2 ) {
	    System.out.println("");
	    System.out.println(" DNA2ORF v." + VERSION + "             Copyright (C) 2021  Institut Pasteur");
	    System.out.println("");
	    System.out.println(" USAGE:  DNA2ORF  -i <infile>  [-l <minlgt>]  [-o <outfile>]");
	    System.out.println("");
	    System.out.println(" OPTIONS:");
	    System.out.println("");
	    System.out.println("   -i <filename>  (mandatory) FASTA-formatted nucleotide sequence file");
	    System.out.println("                  name; should end with .gz when gzipped");
	    System.out.println("   -l <integer>   minimum length of the inferred ORFs (default: 30 bps)");
	    System.out.println("   -o <filename>  output file name (default: standard output)");
	    System.out.println("");
	    System.exit(0);
	}
	
	// ########################################################################
	// ##### reading options                                              #####
	// ########################################################################
	infile = new File(filename=NOTHING); // -i
	minlgt = 30;                         // -l
	outfile = new File(NOTHING);         // -o
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") ) { infile  = new File(filename=args[++o]); continue; }
	    if ( args[o].equals("-o") ) { outfile = new File(args[++o]);          continue; }
	    if ( args[o].equals("-l") ) {
		try { minlgt = Integer.parseInt(args[++o]); }
		catch ( NumberFormatException e ) { System.err.println("incorrect value (option -l): " + args[o]);                     System.exit(1); }
		continue;
	    }
	}
	if ( filename.equals(NOTHING) )           { System.err.println("no input file specified (option -i)");                         System.exit(1); }
	if ( ! infile.exists() )                  { System.err.println("input file does not exist (option -i): " + infile.toString()); System.exit(1); }
	if ( minlgt <= 0 )                        { System.err.println("minimum ORF length should be positive (option -l)");           System.exit(1); }

	// ########################################################################
	// ##### reading infile and ORFing each sequence                      #####
	// ########################################################################
	in = ( filename.endsWith(".gz") )
	    ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER_SIZE)))
	    : Files.newBufferedReader(Path.of(filename));
	out = ( outfile.toString().equals(NOTHING) )
	    ? new BufferedWriter(new OutputStreamWriter(System.out))
	    : Files.newBufferedWriter(Path.of(outfile.toString()));
	line = "";
	try { while ( (line=in.readLine().trim()).length() == 0 ) {} } catch ( NullPointerException e ) {}
	if ( ! line.startsWith(">") ) { System.err.println("incorrectly formatted FASTA file (option -i): " + infile.toString()); System.exit(1); }
	
	lbl = line.substring(1).trim();
	orfSet = new TreeSet<ORF>();
	sb = new StringBuilder("");
	while ( true ) { 
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { line = EOF; }
	    
	    if ( ! line.startsWith(">") ) { sb = sb.append(line); continue; }

	    //## NOTE: lbl = name of the current sequence
	    //## NOTE: seq = current sequence
	    //## NOTE: lgt = length of the current sequence
	    lgt = sb.length();
	    seq = sb.toString().toUpperCase();
	    sb = new StringBuilder("");

	    // ########################################################################
	    // ##### searching for ORFs                                           #####
	    // ########################################################################
	    if ( lgt >= minlgt ) {
		/*orfSet.clear();*/
		
		// ========================================================================================= //
		// ### looking for ORFs in the 6 reading frames                                              //
		// ========================================================================================= //
		//                                                                                           //
		//                            s = position 3 of every codon (from 1 to lgt)                  //
		//            p s             p = s-2                                                        //
		//            | |             mod3(p) = reading frame r of the codon ending at s             //
		//    ... ... ... ... ...     ssr = starting position of a fwd ORF in reading frame r        //
		//                            ser = ending position of a rev ORF in reading frame r          //
		//                                                                                           //
		//  =========== forward ORF ===========              =========== reverse ORF ===========     //
		//                                                                                           //
		//          ss                  p s                          se                  p s         //
		//          |                   | |                          |                   | |         //
		//      TAA |                   TAA                      CTA |                   CTA         //
		//  ... TAG ... ... ... ... ... TAG ...              ... TCA ... ... ... ... ... TCA ...     //
		//      TGA >>> >>> >>> >>> >>> TGA                      TTA <<< <<< <<< <<< <<< TTA         //
		//          |                 |                              |                 |             //
		//          ss                se = s-3                       se                ss = s-3      //
		//                                                                                           //
		//  ORF=[ss,se]   lgt(ORF)=se-ss+1=p-ss              ORF=[ss,se]   lgt(ORF)=ss-se+1=p-se     //
		//                                                                                           //
		// ========================================================================================= //
		//                                                                                           //
		//  Let s such that [ss1,se] is an ORF (with se=s-3):                                        //
		//                                                                                           //
		//  se1                                                                                      //
		//    |   se3                                                                                //
		//    |     |  se2                                                                           //
		//    |     |    | ss2                                                                       //
		//    |     |    |   |  ss3                                                                  //
		//    |     |    |   |    |   ss1------------------------------------se p s                  //
		//    |     |    |   |    |     |                                     | | |                  //
		//  ... ... ... ... ... ... TAG ... ... ... ... ... ... ... ... ... ... TAA ... ... ...      //
		//    |     |    |   |    |     >>> >>> >>> >>> >>> >>> >>> >>> >>> >>>                      //
		//    |     |    |   |    >-------------------------------------------------->               //
		//    |     |    |   >------------------------------------------------------->               //
		//    |     |    <-----------------------------------------------------------<               //
		//    |     <----------------------------------------------------------------<               //
		//    <----------------------------------------------------------------------<               //
		//                                                                                           //
		//  Of note, if ss1 > max(ss2,ss3,se1,se2,se3), then [ss1,se] will be included into one of   //
		//  the putative ORFs starting at position ss2/ss3,  or ending at position se1/se2/se3. In   //
		//  consequence, the ORF [ss1,se] is considered only when ss1 < max(ss2,ss3,se1,se2,se3).    //
		//  Same for ss2, ss3, se1, se2 and se3.                                                     //
		//                                                                                           //
		// ========================================================================================= //
		
		ss1 = 0; ss2 = 1; ss3 = 2; //## NOTE: ORF start indexes for reading frames 1, 2, 3
		se1 = 0; se2 = 1; se3 = 2; //## NOTE: ORF end indexes for reading frames -1, -2, -3
		cs_2 = seq.charAt(0);      //## NOTE: character state at position p = s-2
		cs_1 = seq.charAt(1);      //## NOTE: character state at position s-1
		p = -1; s = 1;             //## NOTE: p = s-2
		while ( ++s < lgt ) {
		    ++p;
		    switch ( (cs=seq.charAt(s)) ) {
		    case 'A': //###################################################################################################################### ..A
			switch ( cs_1 ) {
			case 'A': //================================================================================================================== .AA
			case 'G': //================================================================================================================== .GA
			    if ( cs_2 == 'T' ) //                                                                                                  >>> TAA
				switch ( mod3(p) ) { //                                                                                            >>> TGA
				case 0:  if ( nmf(ss1, ss2, ss3, se1, se2, se3) && (se=p)-ss1 >= minlgt ) orfSet.add(new ORF(ss1, --se)); ss1 = s+1; break;
				case 1:  if ( nmf(ss2, ss1, ss3, se1, se2, se3) && (se=p)-ss2 >= minlgt ) orfSet.add(new ORF(ss2, --se)); ss2 = s+1; break;
				default: if ( nmf(ss3, ss1, ss2, se1, se2, se3) && (se=p)-ss3 >= minlgt ) orfSet.add(new ORF(ss3, --se)); ss3 = s+1; break; }
			    break;
			case 'C': //================================================================================================================== .CA
			    if ( cs_2 == 'T' ) //                                                                                                  <<< TCA
				switch ( mod3(p) ) {
				case 0:  if ( nmf(se1, se2, se3, ss1, ss2, ss3) && (ss=p)-se1 >= minlgt ) orfSet.add(new ORF(--ss, se1)); se1 = s+1; break;
				case 1:  if ( nmf(se2, se1, se3, ss1, ss2, ss3) && (ss=p)-se2 >= minlgt ) orfSet.add(new ORF(--ss, se2)); se2 = s+1; break;
				default: if ( nmf(se3, se1, se2, ss1, ss2, ss3) && (ss=p)-se3 >= minlgt ) orfSet.add(new ORF(--ss, se3)); se3 = s+1; break; }
			    break;
			case 'T': //================================================================================================================== .TA
			    if ( cs_2 == 'C' || cs_2 == 'T' ) //                                                                                   <<< CTA
				switch ( mod3(p) ) { //                                                                                            <<< TTA
				case 0:  if ( nmf(se1, se2, se3, ss1, ss2, ss3) && (ss=p)-se1 >= minlgt ) orfSet.add(new ORF(--ss, se1)); se1 = s+1; break;
				case 1:  if ( nmf(se2, se1, se3, ss1, ss2, ss3) && (ss=p)-se2 >= minlgt ) orfSet.add(new ORF(--ss, se2)); se2 = s+1; break;
				default: if ( nmf(se3, se1, se2, ss1, ss2, ss3) && (ss=p)-se3 >= minlgt ) orfSet.add(new ORF(--ss, se3)); se3 = s+1; break; }
			    break;
			}
		    case 'G': //###################################################################################################################### ..G
			if ( cs_2 == 'T' && cs_1 == 'A' ) //                                                                                       >>> TAG
			    switch ( mod3(p) ) {
			    case 0:      if ( nmf(ss1, ss2, ss3, se1, se2, se3) && (se=p)-ss1 >= minlgt ) orfSet.add(new ORF(ss1, --se)); ss1 = s+1; break;
			    case 1:      if ( nmf(ss2, ss1, ss3, se1, se2, se3) && (se=p)-ss2 >= minlgt ) orfSet.add(new ORF(ss2, --se)); ss2 = s+1; break;
			    default:     if ( nmf(ss3, ss1, ss2, se1, se2, se3) && (se=p)-ss3 >= minlgt ) orfSet.add(new ORF(ss3, --se)); ss3 = s+1; break; }
			break;
		    }
		    cs_2 = cs_1;
		    cs_1 = cs;
		}
		switch ( mod3(p=lgt) ) {
		case 0:  if ( (se=p)-ss1 >= minlgt ) orfSet.add(new ORF(ss1, --se)); if ( (ss=p)-se1 >= minlgt ) orfSet.add(new ORF(--ss, se1));
		    --p; if ( (se=p)-ss3 >= minlgt ) orfSet.add(new ORF(ss3, --se)); if ( (ss=p)-se3 >= minlgt ) orfSet.add(new ORF(--ss, se3));
		    --p; if ( (se=p)-ss2 >= minlgt ) orfSet.add(new ORF(ss2, --se)); if ( (ss=p)-se2 >= minlgt ) orfSet.add(new ORF(--ss, se2)); break;
		case 1:  if ( (se=p)-ss2 >= minlgt ) orfSet.add(new ORF(ss2, --se)); if ( (ss=p)-se2 >= minlgt ) orfSet.add(new ORF(--ss, se2));
		    --p; if ( (se=p)-ss1 >= minlgt ) orfSet.add(new ORF(ss1, --se)); if ( (ss=p)-se1 >= minlgt ) orfSet.add(new ORF(--ss, se1));
		    --p; if ( (se=p)-ss3 >= minlgt ) orfSet.add(new ORF(ss3, --se)); if ( (ss=p)-se3 >= minlgt ) orfSet.add(new ORF(--ss, se3)); break;
		default: if ( (se=p)-ss3 >= minlgt ) orfSet.add(new ORF(ss3, --se)); if ( (ss=p)-se3 >= minlgt ) orfSet.add(new ORF(--ss, se3));
		    --p; if ( (se=p)-ss2 >= minlgt ) orfSet.add(new ORF(ss2, --se)); if ( (ss=p)-se2 >= minlgt ) orfSet.add(new ORF(--ss, se2));
		    --p; if ( (se=p)-ss1 >= minlgt ) orfSet.add(new ORF(ss1, --se)); if ( (ss=p)-se1 >= minlgt ) orfSet.add(new ORF(--ss, se1)); break;
		}
		//System.err.println(lbl); for (ORF orf: orfSet) System.err.println(orf.toString());
	    
		// ############################################################
		// ### shrinking (if required) and outputing ORFs           ###
		// ############################################################
		orfed = new BitSet(lgt); //## NOTE: each ORFed position is set
		o = 0;                   //## NOTE: no. ORFs
		while ( ! orfSet.isEmpty() ) {
		    orf = orfSet.pollLast();
		    if ( (ss=orf.startIndex()) < (se=orf.endIndex()) ) { //## NOTE: fwd ORF
			while ( orfed.get(ss) && (ss+=3) < se ) {}
			while ( orfed.get(se) && (se-=3) > ss ) {}
			if ( (l=se-ss+1) < minlgt )                            continue;   //## NOTE: too short after shrinking
			if ( l != orf.length() ) { orfSet.add(new ORF(ss,se)); continue; } //## NOTE: shrinked
			orfed.set(ss, ++se);
		    }
		    else {                                               //## NOTE: rev ORF
			while ( orfed.get(ss) && (ss-=3) > se ) {}
			while ( orfed.get(se) && (se+=3) < ss ) {}
			if ( (l=ss-se+1) < minlgt )                            continue;   //## NOTE: too short after shrinking
			if ( l != orf.length() ) { orfSet.add(new ORF(ss,se)); continue; } //## NOTE: shrinked
			orfed.set(se, ++ss);
		    }
		    cod = orf.extractFrom(seq);
		    out.write(lbl); out.write('\t'); out.write(orf.toString()); out.write('\t');
		    out.write(cod); out.write('\t'); out.write(dna2aa(cod));    out.newLine();
		    ++o;
		}
		out.flush();
		System.err.println("#" + lbl + "\t" + lgt + String.format(Locale.US, " bps\t%.2f", 100.0*orfed.cardinality()/lgt) + "% translated\t" + o + " ORFs");
		orfed = null;
	    } //## NOTE: end of ORFing of the current sequence
	    
	    if ( line.equals(EOF) ) { in.close(); out.close(); break; } 
	    lbl = line.substring(1).trim();
	}
    }

    // ##### non max first: returns true when the first argument is not greater than the five others #####
    final static boolean nmf(final int x1, final int x2, final int x3, final int x4, final int x5, final int x6) {
	return ! ((x1 > x2) && (x1 > x3) && (x1 > x4) && (x1 > x5) && (x1 > x6));
    }
    
    // ##### fast mod 3 remainder, from Fig. 10-24 in: Warren HS (2013) Hacker's Delight #####
    final static int mod3(final int x) { return (0x55555555*x + (x >>> 1) - (x >>> 3)) >>> 30; }
    
    /* //http://homepage.cs.uiowa.edu/~dwjones/bcd/mod.shtml
       final static int mod3 (final int x) {
       int a = x;
       a = (a >> 16) + (a & 0xFFFF); a = (a >>  8) + (a & 0xFF); a = (a >>  4) + (a & 0xF);
       a = (a >>  2) + (a & 0x3);    a = (a >>  2) + (a & 0x3);  a = (a >>  2) + (a & 0x3);
       return (( a <= 2 ) ? a : a-3);
       } */
    
    // ##### translating DNA sequence String to amino acid String #####
    final static String dna2aa (final String sequence) {
	final int m = sequence.length() / 3; int b1 = -1, b2; StringBuilder sb = new StringBuilder(sequence.substring(0, m));
	while ( ++b1 < m ) { b2 = Arrays.binarySearch(CODON, sequence.substring(3*b1 , 3*(++b1))); sb.setCharAt(--b1 , ((b2 < 0) ? 'X' : AMINO_ACID[b2])); }
	return sb.toString();
    }

    // ##### class dedicated to ORFs #####
    private static class ORF implements Comparable<ORF> {
	private int start;   //## NOTE: both 'start' and 'end' are inclusive
	private int end;     //## NOTE: reverse-complement when 'start' > 'end'
	private int length;
	public ORF(final int start, final int end) {
	    this.start  = start;
	    this.end    = end;
	    this.length = ( start < end ) ? end - start : start - end; this.length++; //this.length = Math.abs(end - start) + 1;
	}
	public int startIndex()  { return this.start;  }
	public int endIndex()    { return this.end;    }
	public int length()      { return this.length; }
	@Override
	public String toString() { return (start+1) + "-" + (end+1); }
	@Override
	public int compareTo(final ORF orf) { return ( this.length >= orf.length() ) ? 1 : -1; } 
	
	public String extractFrom(final String ctg) {
	    return ( this.start < this.end ) ? ctg.substring(this.start, this.end+1) : rc(ctg.substring(this.end, this.start+1));
	}
	
	// ##### transforming DNA sequence String to its reverse complement #####
	final private static String rc (final String nt) {
	    int b1 = nt.length(), b2 = -1; StringBuilder sbrc = new StringBuilder(nt);
	    while ( --b1 >= 0 ) 
		switch ( nt.charAt(b1) ) {
		case 'A': sbrc.setCharAt(++b2 , 'T'); continue;
		case 'C': sbrc.setCharAt(++b2 , 'G'); continue;
		case 'G': sbrc.setCharAt(++b2 , 'C'); continue;
		case 'T': sbrc.setCharAt(++b2 , 'A'); continue;
		default:  sbrc.setCharAt(++b2 , '?'); continue;
		}
	    return sbrc.toString();
	}
    }
}
